#
#  Be sure to run `pod spec lint RDPSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "RDPSDK"
  s.version      = "1.0.8"
  s.summary      = "RDP SDK release"

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  s.description  = "RDP SDK for merchants to integrate with their iOS development"

  s.homepage     = "https://bitbucket.org/reddotpayment/ios-sdk-cocoapod"
  # s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"

  s.license      = { :type => "MIT", :file => "LICENSE" }
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }

  s.author 		= "Red Dot Payment"
  # s.authors           = { "rickyputrah" => "rickyph96@gmail.com" }
  s.social_media_url   	= "https://www.facebook.com/reddotpayment"

  # s.platform     = :ios
  s.platform     = :ios, "9.0"
  s.requires_arc = false

  s.source       = { :git => "https://darylngrdp@bitbucket.org/reddotpayment/ios-sdk-cocoapod.git", :tag => s.version }

  #s.default_subspec = 'Core', 'CardIO'
  s.default_subspec = 'Core'

  s.subspec 'Core' do |subspec|
    #s.source_files =
    #subspec.source_files     = 'RDP-SDK-iOS/Classes/**/*'
    subspec.source_files     = 'RDPSDK/Classes/**/*'
    #subspec.header_mappings_dir = 'RDP-SDK-iOS/Classes/Public'
    #subspec.public_header_files = 'RDP-SDK-iOS/Classes/**/*.h'
    subspec.public_header_files = 'RDPSDK/Classes/**/*.h'
    #subspec.private_header_files = 'RDP-SDK-iOS/Classes/Private/**/*.h'
    #subspec.preserve_path    = [ 'PayPalMobile/*.a' ]
    subspec.frameworks       = 'Accelerate', 'AudioToolbox', 'AVFoundation', 'CoreLocation', 'CoreMedia', 'MessageUI', 'MobileCoreServices', 'SystemConfiguration'
    #subspec.vendored_libraries = [ 'PayPalMobile/libPayPalMobile.a' ]
    subspec.compiler_flags   = '-fmodules'
    subspec.xcconfig         = { 'OTHER_LDFLAGS' => '-lc++ -ObjC'}
    subspec.dependency       'CardIO'
  end

  #s.subspec 'CardIO' do |subspec|
  #end
end
