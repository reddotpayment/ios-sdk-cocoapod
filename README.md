# RDPSDK

[![CI Status](http://img.shields.io/travis/rickyputrah/RDPSDK.svg?style=flat)](https://travis-ci.org/rickyputrah/RDPSDK)
[![Version](https://img.shields.io/cocoapods/v/RDPSDK.svg?style=flat)](http://cocoapods.org/pods/RDPSDK)
[![License](https://img.shields.io/cocoapods/l/RDPSDK.svg?style=flat)](http://cocoapods.org/pods/RDPSDK)
[![Platform](https://img.shields.io/cocoapods/p/RDPSDK.svg?style=flat)](http://cocoapods.org/pods/RDPSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RDPSDK is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "RDPSDK"
```

## Author

Red Dot Payment

## License

RDPSDK is available under the MIT license. See the LICENSE file for more info.
