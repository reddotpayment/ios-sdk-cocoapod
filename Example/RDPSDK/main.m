//
//  main.m
//  RDPSDK
//
//  Created by rickyputrah on 05/24/2017.
//  Copyright (c) 2017 rickyputrah. All rights reserved.
//

@import UIKit;
#import "RDPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RDPAppDelegate class]));
    }
}
